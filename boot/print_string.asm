print_string:
  pusha
print_start:
  mov al, [bx] 
  cmp al, 0
  je end
  call print_char    ;don't forget to use call not jmp
  inc bx
  jmp print_start
end:
  popa
  ret

print_hex:
  pusha
  mov al, [CHAR_0]
  call print_char
  mov al, [CHAR_x]
  call print_char
;  mov dx, bx
  mov cx, dx

  mov cx, dx 
  shr cx, 12 
  and cx, 0xf
  mov bx, CHAR_HEX
  add bx, cx
  mov al, [bx]
  call print_char

  mov cx, dx 
  shr cx, 8 
  and cx, 0xf
  mov bx, CHAR_HEX
  add bx, cx
  mov al, [bx]
  call print_char

  mov cx, dx 
  shr cx, 4 
  and cx, 0xf
  mov bx, CHAR_HEX
  add bx, cx
  mov al, [bx]
  call print_char

  mov cx, dx 
  and cx, 0xf 
  mov bx, CHAR_HEX
  add bx, cx
  mov al, [bx]
  call print_char
  popa
  ret

print_char:
  pusha
  mov ah, 0x0e
  int 0x10
  popa
  ret

;CHAR_HEX: db 'FEDCBA9876543210'
CHAR_HEX: db '0123456789ABCDEF'


CHAR_0: db '0'
CHAR_1: db '1'
CHAR_2: db '2'
CHAR_3: db '3'
CHAR_4: db '4'
CHAR_5: db '5'
CHAR_6: db '6'
CHAR_7: db '7'
CHAR_8: db '8'
CHAR_9: db '9'
CHAR_A: db 'A'
CHAR_B: db 'B'
CHAR_C: db 'C'
CHAR_D: db 'D'
CHAR_E: db 'E'
CHAR_F: db 'F'
CHAR_x: db 'x'
